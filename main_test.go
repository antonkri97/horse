package main

import "testing"

type TestPair struct {
	position          Position
	possiblePositions []Position
}

var tests = []TestPair{
	{
		Position{'d', 4},
		[]Position{
			Position{'f', 5}, Position{'f', 3}, Position{'b', 5}, Position{'b', 3}, Position{'e', 6}, Position{'e', 2}, Position{'c', 6}, Position{'c', 2},
		}},
	{
		Position{'h', 1},
		[]Position{Position{'f', 2}, Position{'g', 3}},
	},
	{
		Position{'a', 8},
		[]Position{Position{'c', 7}, Position{'b', 6}},
	},
	{
		Position{'c', 3},
		[]Position{{'e', 4}, Position{'e', 2}, Position{'a', 4}, Position{'a', 2}, Position{'d', 5}, Position{'d', 1}, Position{'b', 5}, Position{'b', 1}},
	},
}

func TestGetHorseSteps(t *testing.T) {
	for _, pair := range tests {
		positions, err := getHorseSteps(pair.position)
		if err != nil {
			t.Error(err)
		}
		for i, position := range positions {
			if position != pair.possiblePositions[i] {
				t.Errorf("For %c:%d expected %c:%d got %c:%d",
					pair.position.letter,
					pair.position.number,
					pair.possiblePositions[i].letter,
					pair.possiblePositions[i].number,
					position.letter,
					position.number,
				)
			}
		}
	}
}
