package main

import (
	"errors"
	"fmt"
	"math"
	"os"
)

// Position - позиция
type Position struct {
	letter byte
	number int
}

func main() {
	steps, err := getHorseSteps(Position{'c', 3})
	if err != nil {
		fmt.Println("Wrong position")
		os.Exit(-1)
	}

	for _, step := range steps {
		fmt.Printf("%c:%d ", step.letter, step.number)
	}
	fmt.Println()
}

func getHorseSteps(position Position) ([]Position, error) {
	if !checkPosition(position) {
		return nil, errors.New("Bad position")
	}

	steps := produceSteps(position)
	correctSteps := make([]Position, 0)

	for _, step := range steps {
		if checkPosition(step) {
			correctSteps = append(correctSteps, step)
		}
	}

	return correctSteps, nil
}

func produceSteps(position Position) [8]Position {
	var steps [8]Position
	var moves = [4]int{2, -2, 1, -1}
	var s int

	for i := 0; i < 4; i++ {
		for j := 0; j < 4; j++ {
			if math.Abs(float64(moves[i])) != math.Abs(float64(moves[j])) {
				steps[s] = produceStep(position, moves[i], moves[j])
				s++
			}
		}
	}

	return steps
}

func produceStep(position Position, moveLetter, moveNumber int) Position {
	nextL := byte(int(position.letter) + moveLetter)
	nextN := position.number + moveNumber

	return Position{letter: nextL, number: nextN}
}

func checkPosition(position Position) bool {
	letter := int(position.letter)
	number := position.number

	return (letter >= 97 && letter <= 104) && (number >= 1 && number <= 8)
}
